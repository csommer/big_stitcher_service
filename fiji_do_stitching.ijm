// read dataset path, number of tiles as commandline arguments
args = getArgument()
args = split(args, " ");

if (args.length != 5) {
	print("5 args needed: \n\n  1. Path to BDV xml\n  2. Minimum R value\n  3. Downs sample X\n  4. Downs sample Y\n  5. Downs sample Z");
	exit("");
}

baseXML = args[0];
if (!endsWith(baseXML, ".xml"))
{
    print("First arg needs to be bdv .xml file");
	exit("");
}

minR=args[1];

ds_X=args[2];
ds_Y=args[3];
ds_Z=args[4];


print("Args are:");
for (i = 0; i < args.length; i++) {
	print("  -" + args[i]);
}


// calculate pairwise shifts
run("Calculate pairwise shifts ...", "select="+ baseXML +" process_angle=[All angles] process_channel=[All channels] process_illumination=[All illuminations] process_tile=[All tiles] process_timepoint=[All Timepoints] method=[Phase Correlation] downsample_in_x=" + ds_X + " downsample_in_y=" + ds_Y + " downsample_in_z=" + ds_Z + "");


// filter shifts with r corr. threshold
run("Filter pairwise shifts ...", "select="+ baseXML +" filter_by_link_quality min_r="+ minR +" max_r=1 max_shift_in_x=0 max_shift_in_y=0 max_shift_in_z=0 max_displacement=0");

// do global optimization
run("Optimize globally and apply shifts ...",
    "select="+baseXML+" process_angle=[All angles] process_channel=[All channels] " +
    "process_illumination=[All illuminations] process_tile=[All tiles] process_timepoint=[All Timepoints]" +
    " relative=2.500 absolute=3.500 global_optimization_strategy=" +
    "[Two-Round using Metadata to align unconnected Tiles] fix_group_0-0,");

	

