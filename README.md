# BigStitcher Service
---

## Prerequisites

* Install a local ImageJ/Fiji instance into your home folder. The path to ImageJ/Fiji `~/Fiji.app/ImageJ-linux64`
* Make sure the Big Stitcher and N5 update sites are enabled
* Make sure Miniconda is installed (locally) and h5py in the working environment is installed (needed for Dragonfly .ims data). One can also use Python from the cluster module service. (instead of local miniconda) You will need to `pip install h5py --user` in that case.
* For Windows users, it's recommended to use the *MobaXterm*
* The raw images **must** be copied to cluster storage beforehand. Windows users can use WinSCP to copy the data to their home folder on the cluster

## Steps

### Copy raw data
Only cluster login nodes can access fs3 storage. Thus, please, copy the data to you home cluster storage. E.g.

```
cp -rv </fs3/.../img-folder> ~/<img-folder>
```

Windows users can also use WinSCP.

### ~~Unlink pyramid levels~~ 
(not required anymore as of BS>=1.2; Aug. 2023, just update BS)

if dealing with Dragonfly .ims files, the additonal pyramidal scale levels need to be removed from the raw .ims (hdf5) data. (see [post](https://forum.image.sc/t/getting-bigstitcher-to-ignore-resolution-pyramid-series-when-using-bio-formats/57257/6))

``` bash
python unlink_ims_pyramid.py <path to data directory>
```
and follow instructions. This process only takes less than a minute.

### Resave raw data to N5
In order to do efficient stitching, the data needs to be converted to N5.
```
sbatch fiji_create_bs_n5.sbatch <path-to-data-directory> <xy-pixel-size> <z-pixel-size> <Name>
```

### Interactive definition of tile scan and and stitching
Once, data is converted to N5, the stitching can be done relatively fast using Fiji interactively on cluster.

Get an interactive session with many `--cpus-per-task`, e.g. `-c 32`

```bash
srun -N 1 -c 32 --partition gpu --gres=gpu:1 --time=8:00:00 --pty --x11=first --mem=128G bash
# wait a bit till your interactive job is scheduled

# Start Fiji. On Windows, a running XServer needs to be up -> use MobaXterm
~/Fiji.app/ImageJ-linux64
```
Open the `<Name>.xml` in BigStitcher and:
#### 1. Move tiles to regular grid
So far, I have not found a way to use meta data to pre-align the tiles. So one has to use the BigStitcher interactive *Move tiles to regular grid* feature.
1. Select all tiles. Right click -> Arange Views -> Move tiles to regular grid.
2. Choose correct tile configuration X x Y and overlap (sometimes up to 20%)
3. Stitch dataset by right click -> Stitch dataset. Usually, I stitch on 2x or 4x downsampled levels
4. Stitching can sometimes be improved by *Iterative closest point* ICP method.

For more details see the [BigStitcher](https://imagej.net/plugins/bigstitcher/) documentation. 

### Fusing data to HDF5
If fusing is needed you can use the cluster script. Fusing is currently only supported into HDF5.

```bash
sbatch fiji_fuse_bs_to_h5.sbatch <NAME>.xml
```

Note, due to a [bug](https://github.com/PreibischLab/BigStitcher/issues/123) in BigStitcher and a bug in the Andor Dragonfly software, .ims contain different pixel sizes for X and Y. This leads to computational overhead in the fusing step. Before fusing, it's recommended to first fix the (slightly) different pixel sizes manually in the <Name>.xml file.

### Project status
*work in progress*
