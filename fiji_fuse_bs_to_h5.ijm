// read dataset path, number of tiles as commandline arguments
args = getArgument();
args = split(args, " ");

if (args.length != 1) {
	print("First arg needs to be bdv .xml file");
	exit("");
}

baseXML = args[0];
if (!endsWith(baseXML, ".xml"))
{
    print("First arg needs to be bdv .xml file");
	exit("");
}

outHDF = replace(baseXML, ".xml", "-fused.h5");
outHDFXML = replace(baseXML, ".xml", "-fused.xml"); 

print("Export to:");
print(" --" + outHDF);
print(" --" + outHDFXML);

run("Fuse dataset ...", "select="+ baseXML +" process_angle=[All angles] process_channel=[All channels] process_illumination=[All illuminations] process_tile=[All tiles] process_timepoint=[All Timepoints] bounding_box=[All Views] downsampling=1 interpolation=[Linear Interpolation] pixel_type=[16-bit unsigned integer] interest_points_for_non_rigid=[-= Disable Non-Rigid =-] blend preserve_original produce=[All views together] fused_image=[ZARR/N5/HDF5 export using N5-API] define_input=[Auto-load from input data (values shown below)] min=0 max=65535 export=HDF5 create hdf5_file="+ outHDF +" xml_output_file="+ outHDFXML +" viewid_timepointid=0 viewid_setupid=0");