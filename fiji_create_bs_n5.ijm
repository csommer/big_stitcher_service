// read dataset path, number of tiles as commandline arguments
args = getArgument()
args = split(args, " ");

if (args.length != 4) {
	print("4 args needed: \n\n  1. Path to datafolder with .ims files\n 2. Lateral pixelsize\n 3. Voxel depth\n 4. name");
	exit("")
}

basePath = args[0];
if (!endsWith(basePath, File.separator))
{
    basePath = basePath + File.separator;
}

pixel_x = args[1];
pixel_y = args[1];
pixel_z = args[2];

nameXML = args[3];

print("Args are:");
for (i = 0; i < args.length; i++) {
	print("  -" + args[i]);
}


run("Define dataset ...",
    "define_dataset=[Automatic Loader (Bioformats based)] " +
    "project_filename=" + nameXML + ".xml path=" + basePath + "*.ims exclude=10 " +
    "pattern_0=Tiles modify_voxel_size voxel_size_x=" + pixel_x + " " +
    "voxel_size_y=" + pixel_y + " voxel_size_z=" + pixel_z + " voxel_size_unit=micron " +
    "move_tiles_to_grid_(per_angle)?=[Do not move Tiles to Grid (use Metadata if available)] " +
    "keep_metadata_rotation how_to_load_images=[Re-save as multiresolution N5] " +
    "dataset_save_path=" + basePath + " " +
    "subsampling_factors=[{ {1,1,1}, {2,2,2}, {4,4,4}, {8,8,8} }] n5_block_sizes=[{ {128,128,64}, {128,128,64}, {128,128,64}, {128,128,64} }] " +
    "compression=Gzip " +
    "export_path=" + basePath + nameXML);


// run("Define dataset ...",
    // "define_dataset=[Automatic Loader (Bioformats based)]" +
    // " project_filename=dataset.xml path=" + basePath + "*.ims exclude=10" +
    // " pattern_0=Tiles modify_voxel_size voxel_size_x=" + pixel_x + " " +
    // " voxel_size_y=" + pixel_y + " voxel_size_z=" + pixel_z + " voxel_size_unit=micron " +
    // "move_tiles_to_grid_(per_angle)?=[Move Tile to Grid (Macro-scriptable)] grid_type=[Snake: Right & Down      ]" +
    // " tiles_x="+tilesX+" tiles_y="+tilesY+" tiles_z=1 overlap_x_(%)=10 overlap_y_(%)=10 overlap_z_(%)=10" +
    // " keep_metadata_rotation how_to_load_images=[Re-save as multiresolution N5] " +
    // "dataset_save_path=" + basePath + " " +
    // "subsampling_factors=[{ {1,1,1}, {2,2,2}, {4,4,4}, {8,8,8} }] n5_block_sizes=[{ {128,128,64}, {128,128,64}, {128,128,64}, {128,128,64} }] " +
    // "compression=Gzip " +
    // "export_path=" + basePath + "dataset");
	