import os
import sys
import glob
import h5py
from pathlib import Path

def unlink_resolution_levels(fn):
    with h5py.File(fn, "a") as hf:
        n = len(hf["DataSet"])
        print(f"  - Unlink resolution level: {fn}", end=" ")
        for rl in range(1,n):
            del hf[f"DataSet/ResolutionLevel {rl}"]
            print(rl, end=", ")
        print("Done")
        
        
if __name__ == "__main__":
    if len(sys.argv) != 2 :
        print("Please supply folder for unlinking .ims pyramids")
        exit()
    folder = sys.argv[1]
    exists = Path(folder).exists()
    if not exists:
        print(f"Folder '{folder}' does not exist")
        exit()
        
    ims_fns = list(glob.glob(folder + "/*.ims"))
    proceed = input(f"Unlink {len(ims_fns)} .ims in  '{folder}' ? [y/n]")
    if proceed.lower().startswith("y"):
        for fn in ims_fns:
            unlink_resolution_levels(fn)
    
    
    
    